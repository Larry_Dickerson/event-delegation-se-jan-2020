/*
X 1. Click on a record in the display rack to add it to the cart.
    X a. Click handler to select the record
    X b. Copy that record into the cart div
        ?? How do we make a copy of a node?
        - event.target.cloneNode(true)
        ?? How do we move a node from one place in the DOM to another?
        - cartEl.appendChild(event.target)
X 2. Visually indicate that the record is selected.
    X a. highlight? border?
3. Click a record in the cart to remove it from the cart.
    a. click handler on the items in the cart
    b. Unselect that record in the display rack
    c. Remove the record from cart
*/

const addRecordToCart = function (evt) {
    if (evt.target.className != "record") {
        return
    }
    
    const recordEl = evt.target

    // copy record to cart
    const recordElCopy = recordEl.cloneNode(true)
    const cartEl = document.querySelector('#cart')
    cartEl.appendChild(recordElCopy) // copy the record

    // style the selected record
    recordEl.className += ' selected'
}

const removeRecordFromCart = function (evt) {
    if (evt.target.className != 'record') {
        return
    }

    const recordStockNum = evt.target.dataset.stocknum
    console.log(recordStockNum)
    // remove the node from cartEl
    const cartEl = document.querySelector('#cart')
    cartEl.removeChild(evt.target)
    // remove selected style from corresponding record in the rack
    const recordInRack = document.querySelector('.record[data-stocknum="' + recordStockNum + '"')
    recordInRack.className = 'record'
}

let displayRack = document.querySelector("#main")
displayRack.addEventListener('click', addRecordToCart)

const cartEl = document.querySelector('#cart')
cartEl.addEventListener('click', removeRecordFromCart)